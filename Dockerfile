FROM python:3.13.0a5-slim-bullseye
COPY server.py .
COPY config.json .
EXPOSE 9998
CMD ["python", "-u", "server.py"]